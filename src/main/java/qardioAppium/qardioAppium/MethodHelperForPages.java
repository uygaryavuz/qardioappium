package qardioAppium.qardioAppium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MethodHelperForPages {
	
		public AndroidDriver<MobileElement> driver = null;
		public WebDriverWait wait = null;
		
		// Constructor
		public MethodHelperForPages(AndroidDriver<MobileElement> driver, WebDriverWait wait) {
			this.driver = driver;
			this.wait = wait;
		}
		
		//Methods

		//wait method for all elements
		public WebElement waitUntilVisibility(By elementlocation) {
			return wait.until(ExpectedConditions.elementToBeClickable(elementlocation));
		}
		//click method
		public void click(By elementlocation) {
			waitUntilVisibility(elementlocation).click();
			
		}
		
		//write text method
		public void writeText(By elementlocation, String texttowrite) {
			waitUntilVisibility(elementlocation);
			driver.findElement(elementlocation).setValue(texttowrite);
		}
		
		//read text method
		public String readText(By elementlocation) {
			return waitUntilVisibility(elementlocation).getText();
		}
		
		//check if an element exist method
		public boolean checkIfExist(By elementlocation) {
			if(driver.findElements(elementlocation).size() != 0) {
				return true;
			}
			else 
				return false;
		}
}
