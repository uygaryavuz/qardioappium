package qardioAppium.qardioAppium.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.TouchAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import qardioAppium.qardioAppium.MethodHelperForPages;


public class HomePage extends MethodHelperForPages{

	public HomePage(AndroidDriver<MobileElement> driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	//Locators
	By allowlocationbtn = (By.id("com.android.packageinstaller:id/permission_allow_button"));  //allow locations allow button
    By qardiobaselinkathomepage = (By.id("com.getqardio.android:id/qardio_base_details")); //qardiobase link at middle of the home page
	By genericandroidbtn = (By.id("android:id/button1")); //warning accept/ OK button
    
    
    
	//Methods
    
  //Click and accept Location Permission (For handling different location warnings)
    public void allowLocation() {
    	if(checkIfExist(allowlocationbtn)) {
        	click(allowlocationbtn);
        	click(genericandroidbtn);
        	click(genericandroidbtn);
        }
    	else if(checkIfExist(genericandroidbtn)) {
    		click(genericandroidbtn);
        	click(genericandroidbtn);
    	}
    	else {		
    	}
    	
    }
    
    //a workaround method when appium can't get the source of elements after closing pop-up
    public void runinBackground() {
    	try {
    		driver.runAppInBackground(Duration.ofSeconds(1));
    	}catch (Exception e) {
    		e.printStackTrace();
		}
    }
    
    //go to QardioBase page
    public void gotoQardioBasePage() {
    	if(checkIfExist(qardiobaselinkathomepage)) {
    		click(qardiobaselinkathomepage);
    	}

    	else {    //if cant get the source elements, workaround
    		runinBackground();  //a workaround method explained above
    		click(qardiobaselinkathomepage);
    	}
	}
	
}


