package qardioAppium.qardioAppium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import qardioAppium.qardioAppium.MethodHelperForPages;

public class QardioBasePage extends MethodHelperForPages{

	public QardioBasePage(AndroidDriver<MobileElement> driver, WebDriverWait wait) {
		super(driver, wait);

	}
	//Elements Locators
	By closeintroversion = (By.id("com.getqardio.android:id/btnCloseOnboarding")); //close intro version modal
	By exitsetupwarning = (By.id("android:id/button1"));
	By addmanualmeasurementbtn = (By.id("com.getqardio.android:id/action_add_weight_measurement")); //qardiobase page above add measurement button
	
	By weightinputarea = (By.id("com.getqardio.android:id/weight_value")); 
	By fatpercinputarea = (By.id("com.getqardio.android:id/fat_percentage"));
	By musclepercinputarea = (By.id("com.getqardio.android:id/muscle_percentage"));
	By waterpercinputarea = (By.id("com.getqardio.android:id/water_percentage"));
	By bonepercinputarea = (By.id("com.getqardio.android:id/bone_percentage"));
	By noteinputarea = (By.id("com.getqardio.android:id/note_input"));  //add note area
	By savemeasurementbtn = (By.id("com.getqardio.android:id/action_save"));  //save measurement button
	
	By showhistorytab = (By.id("com.getqardio.android:id/history_image")); //locator of history in QardioBase Page
	By measurementadddate = (By.id("com.getqardio.android:id/base_measurement_date")); //measurement detail page, date locator at left head
	
	//Page Variables
	String firstelementofhistorypage = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ListView/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup";
	
	//Methods
    //Close modal which ask version info
	public void closeIntroVersion() {
    	click(closeintroversion);
    	click(exitsetupwarning);
    }
    
	//add manual measurement values
    public void addManualMeasurement() {
    	click(addmanualmeasurementbtn);
    
    	writeText(weightinputarea, "80");
    	writeText(fatpercinputarea, "17");
    	writeText(musclepercinputarea, "50");
    	writeText(waterpercinputarea, "45");
    	writeText(bonepercinputarea, "15");
    	
    	click(noteinputarea);
    	writeText(noteinputarea, "getup-standup-dont giveup the fight");
        driver.navigate().back();
        driver.navigate().back();
        click(savemeasurementbtn);
          
    }
    
    //Verify if last values entered is saved
    public void assertAddingMeasurementFromHistory() {
    	click(showhistorytab);
    	click(By.xpath(firstelementofhistorypage));
    	Assert.assertEquals(readText(measurementadddate),"Today");
    }
}
