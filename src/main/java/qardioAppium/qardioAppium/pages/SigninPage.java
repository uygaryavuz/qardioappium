package qardioAppium.qardioAppium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import qardioAppium.qardioAppium.MethodHelperForPages;


public class SigninPage extends MethodHelperForPages{

	public SigninPage(AndroidDriver<MobileElement> driver, WebDriverWait wait) {
		super(driver, wait);
	}

	//Locators
    By emailinputarea = (By.id("com.getqardio.android:id/email_edit"));  //email input area in sign in page
    By passwordinputarea = (By.id("com.getqardio.android:id/password_edit"));  //password input area in signin page
    By signinbtn = (By.id("com.getqardio.android:id/login_button"));  //signin/submit button in signin page
	
	//Methods
    
    //signin with valid inputs
	public void signIn(String email, String pass) {
		writeText(emailinputarea, email);
		writeText(passwordinputarea, pass);
		click(signinbtn);
	}
}
