package qardioAppium.qardioAppium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import qardioAppium.qardioAppium.MethodHelperForPages;


public class WelcomePage extends MethodHelperForPages {
	//Constructor
	public WelcomePage(AndroidDriver<MobileElement> driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	//Locators
	By signinlink = (By.id("com.getqardio.android:id/right_link"));  //sign in link at right bottom at welcome page
	
	//Methods
	public void gotoSignin() {
		click(signinlink);
	}
}
