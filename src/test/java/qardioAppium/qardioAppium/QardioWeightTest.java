package qardioAppium.qardioAppium;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import qardioAppium.qardioAppium.pages.HomePage;
import qardioAppium.qardioAppium.pages.QardioBasePage;
import qardioAppium.qardioAppium.pages.SigninPage;
import qardioAppium.qardioAppium.pages.WelcomePage;

public class QardioWeightTest extends TestBase{
	 
    @Test(groups = {"First2Run"},enabled = true, retryAnalyzer = RetryFailedTests.class, description = "Signin And Add Measurement")
    public void SigninAndAddMeasurement () {
    	ReadFileData readFileData = new ReadFileData();
    	WelcomePage welcomePage = new WelcomePage(driver, wait);
    	SigninPage signinPage = new SigninPage(driver, wait);
    	HomePage homePage = new HomePage(driver, wait);
    	QardioBasePage qardioBasePage = new QardioBasePage(driver, wait);
    	readFileData.readFile();
    	
    	test = extent.createTest("Signin And Add Measurement");
        welcomePage.gotoSignin();
        signinPage.signIn(getSysPropty("username"), getSysPropty("password"));
        test.log(Status.INFO, "Signedin Successfully");
        homePage.allowLocation();
        homePage.gotoQardioBasePage();
        qardioBasePage.closeIntroVersion();
        qardioBasePage.addManualMeasurement();
        qardioBasePage.assertAddingMeasurementFromHistory();
        test.log(Status.INFO, "Measurement Added Successfully");
		
    }
}
