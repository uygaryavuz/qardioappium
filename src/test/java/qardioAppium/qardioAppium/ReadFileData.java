package qardioAppium.qardioAppium;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadFileData {

	String workingDir = System.getProperty("user.dir");
	public void readFile() {
		File file = new File(workingDir+"\\src\\Properties\\dataFile.properties");
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);	
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		
		//load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.setProperty("username",prop.getProperty("username"));
		System.setProperty("password",prop.getProperty("password"));
		
	}
	
	

}
