package qardioAppium.qardioAppium;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


public class TestBase extends ReportProvider{

	@BeforeTest
    //@Parameters(value = {"devicename"})  //Can get from testng.xml file as parameters
    public void setup () throws MalformedURLException{
    	
    	//For turning into a more dynamic way like run from cmd or bash giving parameters
    	String deviceName = System.getProperty("deviceName");
    	//deviceName = "Pixel 2 API 26";
    	String udid = System.getProperty("udid");
    	//udid = "emulator-5554";
    	String platformVersion = System.getProperty("platformVersion");
    	//platformVersion = "8.0";
    	if(udid == null) {
    		System.out.println("Device parameters can't get from command line");
    	}
    	DesiredCapabilities caps = new DesiredCapabilities();
    	caps.setCapability("deviceName", deviceName);  //need to give a name to device like Oneplus 6T etc
    	caps.setCapability("udid", udid); //DeviceId from "adb devices" command
    	caps.setCapability("platformVersion", platformVersion);  //android version like 8.0 or 9.0.6
    
        caps.setCapability("platformName", "Android");
        caps.setCapability("autoGrantPermissions", true); 
        caps.setCapability("skipUnlock","true");
        caps.setCapability("appPackage", "com.getqardio.android");
        caps.setCapability("appActivity","com.getqardio.android.ui.activity.SplashActivity");
        caps.setCapability("noReset","false");
        
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
        wait = new WebDriverWait(driver, 10);
        
        createFolderforScreenshots();
        extentReport(deviceName);
    }
 
	//Runs before each test case's occurance
	@BeforeMethod
	public void beforeMethod() {
		
	}
		
	//Runs After each test case's occurance
	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		reportResultGenerator(result);
		driver.resetApp();
	}
	//Runs after finish of whole tests
	@AfterTest
	public void afterTest() {
		extent.flush();
		driver.quit();
	 }
}

